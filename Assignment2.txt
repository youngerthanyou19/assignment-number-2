Assignment 2

1) What is persistence in IOS?

What I mean by persistence is to make data that�s in your apps stay around between application launches. Persistence lets
users store persistent data and also retrieve it, so that users don�t have to reenter all their data each time they use
their applications. There are multiple ways to store data in iOS devices but most of them aren�t good enough to store a
complicated data. They are usually used to save settings or to preload some data such as �Property List� and �Archiving
Objects�. So that�s why we�ll go through Core Data to see how you can utilize it to manage data in database.

2)What are the different ways to persist data in iOS?

Using persistent stores - Access to stores is mediated by an instance of NSPersistentStoreCoordinator. You should not need
to directly access a file containing a store. From a persistent store coordinator, you can retrieve an object that
represents a particular store on disk. Core Data provides an NSPersistentStore class to represent persistent stores.